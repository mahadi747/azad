<?php require_once "header.php";?>
<body>
   <!-- Hero Section Start From Here  -->
    <?php require_once "template-parts/common/hero.php";?>
    <!-- Hero  Section End Here  -->

    <!-- Slider Section Start Here  -->
    <?php include_once "template-parts/home-page/slider.php";?>
    <!-- Slider Section End Here  -->


    <!-- Recent Post Section  -->
    <?php include_once "template-parts/home-page/recentPost.php";?>
    <!-- Recent Post Section End -->

    <!-- Big Post Section Start Here  -->
    <div class="container">
        <div class="row mb-30">
            <?php include_once "template-parts/home-page/featurePost.php"; ?>

            <!-- Left Sidebar / Aside Section Start Here  -->
            <?php include_once "template-parts/common/sidebar.php";?>

        </div>
    </div>
    <!-- Big Post Section End Here  -->

   <?php require_once "footer.php";?>
