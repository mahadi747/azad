<?php require_once "header.php"; ?>
    <body>
<!-- Hero Section Start From Here  -->
<?php require_once "template-parts/common/hero.php"; ?>
<!-- Hero  Section End Here  -->

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-30 p-1">Contact us</h1>
            <p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut delectus facilis fuga illo
                illum, impedit, iste modi mollitia neque nulla possimus quas quidem recusandae repudiandae soluta
                temporibus unde voluptas, voluptatum.</p>
            <p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid nisi nulla
                pariatur quod! Aperiam, at cupiditate dignissimos dolores esse expedita iusto quis quod reiciendis
                similique? Ab, animi corporis culpa delectus distinctio eos illo sapiente? Laudantium molestiae quasi
                quo ratione? Facilis optio, voluptate. Adipisci aliquid asperiores autem consectetur cupiditate debitis
                delectus dolore ea esse est eum explicabo fugit hic id iste iure labore nihil nisi odio praesentium quas
                quasi quibusdam quisquam ratione recusandae sapiente tenetur ullam velit, voluptate voluptatem? Ad
                beatae delectus dolore ducimus laboriosam minima necessitatibus nisi quas tempora vitae. Consequuntur
                ducimus esse ipsa iste laudantium necessitatibus quidem, tempora.</p>
            <p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem cupiditate
                distinctio dolor dolore dolores eveniet fuga in laboriosam molestias nihil odit possimus, voluptatum.
                Deleniti harum mollitia obcaecati, quae quos sit. Accusantium aliquam, autem blanditiis consequatur
                culpa cumque doloremque dolorum est facilis fuga fugiat impedit inventore itaque iusto laboriosam magnam
                magni, maiores molestiae mollitia non odit officia perferendis possimus praesentium provident qui saepe
                sint tempore temporibus tenetur vel velit voluptate, voluptatibus. Animi esse ipsum laudantium mollitia
                possimus quod soluta. Adipisci, animi cumque delectus distinctio ducimus et eveniet expedita facilis
                illum, ipsam, itaque necessitatibus obcaecati odit quae quas repellendus sunt voluptate?</p>
            <p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet animi atque beatae
                blanditiis culpa dolor dolorem dolores, eius enim, eum fugit harum inventore magnam modi nemo nisi
                numquam odit perferendis perspiciatis quasi quod, reiciendis reprehenderit sequi ut vel vero. Ab ad
                aspernatur autem, cum cumque dolorem enim eum officia officiis perferendis, perspiciatis possimus quis
                unde ut voluptatum? Asperiores dolorum exercitationem fuga quae unde. A, ad cupiditate dicta esse
                laboriosam maiores obcaecati quisquam suscipit unde. Alias, amet cupiditate deserunt eligendi ex in odit
                quasi quis, repellat reprehenderit soluta sunt vitae? Ab aut dicta, excepturi exercitationem impedit
                inventore magnam maiores modi, necessitatibus nihil nobis nostrum possimus, praesentium provident quae
                similique suscipit temporibus ullam velit voluptate. A amet asperiores atque cum cupiditate earum eum
                eveniet ex fugit hic id, inventore itaque laboriosam magni molestias nesciunt nobis, officiis placeat
                quasi quisquam ratione recusandae reiciendis reprehenderit repudiandae suscipit, tempora temporibus
                ullam veritatis voluptates voluptatibus. Debitis ducimus ea eum expedita molestiae. Ad alias aliquam
                aliquid consequatur cupiditate, deleniti, dolor, eaque hic libero magni maxime neque perferendis
                perspiciatis quam quisquam rerum similique suscipit tempora ullam voluptatibus. A corporis cum, cumque
                et ex labore, mollitia natus quas recusandae reiciendis sed tenetur, totam. Beatae ipsa laboriosam quo
                ratione!</p>
        </div>
        <?php include_once "template-parts/common/sidebar.php"; ?>
    </div>
</div>
<?php require_once "footer.php"; ?>