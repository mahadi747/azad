<?php
/**
 * Created by PhpStorm.
 * User: Azad Hossain
 * Date: 6/27/2021
 * Time: 3:36 AM
 */

class recentPosts
{
    public $recentPost = array(
        array(
            'title'=>'This is Post Recent 01',
            'image'=>'assets/img/225x80.png',
            'link'=>'content.php'
        ),
        array(
            'title'=>'This is Post Recent 02',
            'image'=>'assets/img/225x80.png',
            'link'=>'content.php'
        ),
        array(
            'title'=>'This is Post Recent 03',
            'image'=>'assets/img/225x80.png',
            'link'=>'content.php'
        ),
        array(
            'title'=>'This is Post Recent 04',
            'image'=>'assets/img/225x80.png',
            'link'=>'content.php'
        ),
    );
}