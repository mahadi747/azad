<?php 

class mainMenu{
    public $list = array();
    function __construct()
    {
        $this->list = [
            ['name'=>'Home','link'=>'index.php'],
            ['name'=>'Service','link'=>'service.php'],
            ['name'=>'Gallery','link'=>'gallery.php'],
            ['name'=>'About us','link'=>'about.php'],
            ['name'=>'Contact us','link'=>'contact.php'],
        ];
    }
    function getMenu(){
        return $this->list;
    }
}