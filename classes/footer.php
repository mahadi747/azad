<?php
/**
 * Created by PhpStorm.
 * User: Azad Hossain
 * Date: 6/27/2021
 * Time: 5:06 AM
 */

class footer
{
    public $site_credit = "Copyright © 2013 Domain Name - All Rights Reserved";
    public $template_credit = "Template by OS Templates";
    public $subscribtions = array(
        [
            'hint' => 'Please enter your email to join our mailing list',
            'unsubscibe' => 'To unsubsribe please click here',
            'unsubscribeLike' => '#'
        ]
    );
    public $footer_post = array(
        array(
            'content' => 'Lorem ipsum dolor Suspendisse in neque Praesent et eros Lorem ipsum dolor Suspendisse in neque',
            'title' => 'Lacus interdum'
        ),
        array(
            'content' => 'Lorem ipsum dolor Suspendisse in neque Praesent et eros Lorem ipsum dolor Suspendisse in neque',
            'title' => 'Lacus interdum'
        ),
        array(
            'content' => 'Lorem ipsum dolor Suspendisse in neque Praesent et eros Lorem ipsum dolor Suspendisse in neque',
            'title' => 'Lacus interdum'
        )
    );
}