<?php
/**
 * Created by PhpStorm.
 * User: Azad Hossain
 * Date: 6/27/2021
 * Time: 3:47 AM
 */

class featurePosts
{
    public $featPost = array(
        array(
            'image' => 'assets/img/300x150.png',
            'title' => 'Indonectetus facilis leo nibh',
            'content' => "Morbitincidunt maurisque eros molest nunc anteget sed vel lacus mus semper.
                          Anter dumnullam interdum eros dui urna consequam ac nisl nullam ligula vestassa. 
                          <br> <br>
                          Condimentumfelis et amet tellent quisquet a leo lacus nec augue accumsan. 
                          Sagittislaorem dolor ipsum at urna et pharetium malesuada nis consectus odio. ",
            'link' => 'content.php'
        ),
        array(
            'image' => 'assets/img/300x150.png',
            'title' => 'Indonectetus facilis leo nibh',
            'content' => "Morbitincidunt maurisque eros molest nunc anteget sed vel lacus mus semper.
                          Anter dumnullam interdum eros dui urna consequam ac nisl nullam ligula vestassa. 
                          <br> <br>
                          Condimentumfelis et amet tellent quisquet a leo lacus nec augue accumsan. 
                          Sagittislaorem dolor ipsum at urna et pharetium malesuada nis consectus odio. ",
            'link' => 'content.php'
        ),
        array(
            'image' => 'assets/img/300x150.png',
            'title' => 'Indonectetus facilis leo nibh',
            'content' => "Morbitincidunt maurisque eros molest nunc anteget sed vel lacus mus semper.
                          Anter dumnullam interdum eros dui urna consequam ac nisl nullam ligula vestassa. 
                          <br> <br>
                          Condimentumfelis et amet tellent quisquet a leo lacus nec augue accumsan. 
                          Sagittislaorem dolor ipsum at urna et pharetium malesuada nis consectus odio. ",
            'link' => 'content.php'
        ),
        array(
            'image' => 'assets/img/300x150.png',
            'title' => 'Indonectetus facilis leo nibh',
            'content' => "Morbitincidunt maurisque eros molest nunc anteget sed vel lacus mus semper.
                          Anter dumnullam interdum eros dui urna consequam ac nisl nullam ligula vestassa. 
                          <br> <br>
                          Condimentumfelis et amet tellent quisquet a leo lacus nec augue accumsan. 
                          Sagittislaorem dolor ipsum at urna et pharetium malesuada nis consectus odio. ",
            'link' => 'content.php'
        ),
    );
}