<?php
    include_once "classes/sidebar.php";
    $sidebar_obj = new sidebar();
    $posts = $sidebar_obj->posts;
?>
<aside class="col-4 pl-30 mt-30">
    <h2>Ametorci Phasellus</h2>
    <?php
        foreach ($posts as $post){
    ?>
    <article class="sidebar-post">
        <img class="thumbnail" src="<?php echo $post['image'];?>" alt="Image">
        <div class="sidebar-post-body">
            <h4><?php echo $post['title'];?></h4>
            <p><?php echo $post['content'];?></p>
            <a href="<?php echo $post['link'];?>" class="continue-read">Continue Reading »</a>
        </div>
    </article>
    <?php } ?>
    <h2 class="mt-30">Ametorci Phasellus</h2>
    <div class="gallery">
        <?php
            $images = $sidebar_obj->image;
            foreach ($images as $image){
        ?>
        <img class="thumbnail" src="<?php echo $image;?>" alt="Image">
        <?php } ?>
    </div>
</aside>