 <!-- Header Top Start Here -->
 <?php
 include "classes/headerTopInfo.php";
    $headerTopInfo_obj = new headerTopInfo();
 ?>
 <div class="container">
        <div class="row ptb-35">
            <div class="col-7">
                <h1 class="site-title tc-1"><a href="index.php"><?php echo $headerTopInfo_obj->title;?></a></h1>
                <h4 class="site-description tc-1"><?php echo $headerTopInfo_obj->tagline; ?></h4>
            </div>
            <div class="col-5 text-right">
                <ul class="nav-top">
                    <?php 
                    $lists = $headerTopInfo_obj->list;
                    $total_item = count($lists);
                        foreach($lists as $index => $item){
                            $sl = $index + 1;
                    ?>
                    <li class="nav-top-item"><a href="<?php echo $item['link']; ?>" class="nav-top-link tc-1" target="_blank"><?php echo $item['name']; ?></a></li>
                    <?php
                     if($sl !=  $total_item){
                        echo '|';
                    }
                    ?>
                    <?php }?>
                </ul>
                <div class="contact-menu">
                    <span class="tc-1">Tel: <?php echo $headerTopInfo_obj->tel; ?></span> |
                    <span class="tc-1">Mail: <?php echo $headerTopInfo_obj->mail; ?></span>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Top End Here -->


    <!-- Navbar Section Start Here  -->
    <div class="container h-57">
        <div class="row navbar bg-1">
 <?php 
    include "classes/mainMenu.php";
    $menuObj = new mainMenu();
    $menus = $menuObj-> getMenu();
 ?>
            <nav class="col-7">
                <ul class="navbar-nav">
                    <?php 
                        foreach($menus as $menu){
                    ?>
                    <li class="nav-item"><a href="<?php echo $menu['link']; ?>" class="nav-link"><?php echo $menu['name']; ?></a></li>
                    <?php } ?>
                </ul>
            </nav>
            <div class="col-5">
                <div class="search-bar text-right">
                    <input type="search" placeholder="Search this website..." name="search">
                    <input type="submit" value="SEARCH" name="submit">
                </div>
            </div>
        </div>
    </div>