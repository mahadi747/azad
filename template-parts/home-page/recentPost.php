<?php
include 'classes/recentPosts.php';
$recentPost_obj = new recentPosts();
$posts          = $recentPost_obj->recentPost;
?>
<div class="container">
    <div class="row mt-20">
        <?php
        foreach ($posts as $post) {
            ?>
            <div class="col-3 recent-post">
                <a href="<?php echo $post['link']; ?>">
                    <img src="<?php echo $post['image']; ?>" alt="Image" class="recent-post-img">
                </a>
                <a href="<?php echo $post['link']; ?>">
                    <h5 class="recent-post-title bg-1">
                        » <?php echo $post['title']; ?>
                    </h5>
                </a>
            </div>
        <?php } ?>
    </div>
</div>