<?php
include 'classes/featurePosts.php';
 $feat_post_obj = new featurePosts();
 $feat_posts = $feat_post_obj->featPost;
?>
<div class="col-8 d-flex">
    <?php
        foreach ($feat_posts as $post){
    ?>
    <article class="big-post mt-30">
        <h2><?php echo $post['title'];?></h2>
        <img src="<?php echo $post['image'];?>" alt="Image">
        <p><?php echo $post['content'];?></p>
<!--        <p>--><?php //echo $post['content2'];?><!--</p>-->
        <a href="<?php echo $post['link'];?>" class="continue-read">Continue Reading »</a>
    </article>
    <?php } ?>
</div>