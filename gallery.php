<?php require_once "header.php"; ?>
    <body>
<!-- Hero Section Start From Here  -->
<?php require_once "template-parts/common/hero.php"; ?>
<!-- Hero  Section End Here  -->

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-30 p-1">Gallery</h1>
            <div class="flex-container">
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div><div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>
                <div class="flex-item">
                    <img src="assets/img/80x80.png" alt="">
                </div>

            </div>
        </div>
        <?php include_once "template-parts/common/sidebar.php"; ?>
    </div>
</div>
<?php require_once "footer.php"; ?>