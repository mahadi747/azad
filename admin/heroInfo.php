<?php require_once "header.php"; ?>
    <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
     data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
<?php include_once "template-parts/common/hero.php"; ?>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
<?php include_once "template-parts/common/leftSidebar.php"; ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Hero Information</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <div class="d-md-flex">
                        <ol class="breadcrumb ms-auto">
                            <li><a href="#" class="fw-normal">Dashboard</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal form-material">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Website Title</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" placeholder="write Here your Website Tittle"
                                               class="form-control p-0 border-0">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Website Tageline</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="text" placeholder="write Here your Website Tagline"
                                               class="form-control p-0 border-0">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Website Contact Phone Number</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="tel" placeholder="Write Here your Website Contact Phone Number"
                                               class="form-control p-0 border-0">
                                    </div>
                                </div>

                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Website Contact Email Address</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="email" placeholder="Write Here your Website Contact Email Address"
                                               class="form-control p-0 border-0">
                                    </div>
                                </div>

                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Header Top Menu</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <!-- Repeater Add Btn -->
                                        <div id="repeater" class="ps-lg-5">
                                            <!-- Repeater Heading -->
                                            <div class="repeater-heading">
                                                <h3 class="float-start">Repeater</h3>
                                                <button type="button"
                                                        class="btn btn-primary repeater-add-btn float-end">
                                                    Add
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- Repeater Items -->
                                            <div class="items" data-group="test">
                                                <!-- Repeater Items Here -->
                                                <div class="item-content">
                                                    <div class="form-group">
                                                        <label for="item" class="col-lg-2 control-label">Item</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control ps-3" id=item" placeholder="write item name" data-name="item">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="link" class="col-lg-2 control-label">Link</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control ps-3" id="link" placeholder="Insert Here Item Hyper Link" data-skip-name="true" data-name="link">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Repeater Remove Btn -->
                                                <div class="float-end repeater-remove-btn">
                                                    <button type="button" id="remove-btn" class="btn btn-danger"
                                                            onclick="$(this).parents('.items').remove()">
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="items" data-group="test">
                                                <!-- Repeater Items Here -->
                                                <div class="item-content">
                                                    <div class="form-group">
                                                        <label for="item1" class="col-lg-2 control-label">Link Item</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control ps-3" id="item1" placeholder="write item name" data-name="link">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="link1" class="col-lg-2 control-label">Link <span class="text-muted">#</span></label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control ps-3" id="link1" placeholder="Insert Here Item Hyper Link" data-skip-name="true" data-name="link">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Repeater Remove Btn -->
                                                <div class="float-end repeater-remove-btn">
                                                    <button type="button" id="remove-btn" class="btn btn-danger"
                                                            onclick="$(this).parents('.items').remove()">
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Save Changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
<?php require_once "footer.php"; ?>