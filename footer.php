<!-- Footer Section Start Here  -->
<?php
include_once "classes/footer.php";
$footer_obj = new footer();
?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="footer-top">
                <div class="footer-top-section single">
                    <h2>Stay In The Know !</h2>
                    <?php
                    $subcribtion = $footer_obj->subscribtions;
                    foreach ($subcribtion as $item) {
                    ?>
                    <p><?php echo $item['hint']; ?></p>
                    <input type="search" name="search">
                    <input type="submit" value="GO" name="submit">
                    <p><a href="<?php echo $item['unsubscribeLike']; ?>"><?php echo $item['unsubscibe']; ?> »</a></p>
                    <?php }?>
                </div>
                <?php
                $footer_posts = $footer_obj->footer_post;
                foreach ($footer_posts as $post) {
                    ?>
                    <div class="footer-top-section">
                        <h2><?php echo $post['title']; ?></h2>
                        <p><?php echo $post['content']; ?></p>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="col-12 bg-1">
            <footer>
                <p class="copyright-text"><?php echo $footer_obj->site_credit; ?></p>
                <p class="author-text"><?php echo $footer_obj->template_credit; ?></p>
            </footer>

        </div>
    </div>
</div>
<!-- Footer Section End Here  -->
</body>

</html>
